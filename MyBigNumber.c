#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

// reverseNum(stn) will reverse the order of the numbers in the original 
// string
// requires: stn is a valid string
// effect: allocates memory (user must free)
// time: O(n), where n is the length of the string
static char *reverseNum(const char *stn) {
    assert(stn);
    int len = strlen(stn)+1;
    char *new = malloc(len*sizeof(char));
    for (int i = 0; i < len-1; i++) {
        new[i] = stn[len-2-i];
    }
    new[len-1] = '\0';
    return new;
}


// getDigit(stn, idx) will get the digit of the string number stn at idx
// requires: stn is a valid string
// time: O(1)
static int getDigit(const char *stn, int idx) {
    assert(stn);
    int len = strlen(stn);
    if (idx < strlen(stn)) {
        return stn[idx]-'0';
    }
    return 0;
}

// printLog(step, digit_1, digit_2, carry, saved) will output all the
// operation perform when adding two digits together
// requires: step >= 1, the two digits must be from 0 to 9
// effect: produce output
// time: O(1)
static void printLog(int step, int digit_1, int digit_2, 
                int *carry, int saved) {

    assert(step >= 1);
    assert(digit_1 >= 0 && digit_1 <= 9);
    assert(digit_2 >= 0 && digit_2 <= 9);

    int c = *carry;
    int sum = digit_1 + digit_2 + c;
    printf("Bước %d: ", step);
    printf("Lấy %d cộng với %d được %d.", digit_1, digit_2, sum-c);
    if (*carry == 1) {
        printf(" Cộng tiếp với nhớ 1 được %d", sum);
    }

    int sum_digit = 0;
    if (sum > 9) {
        sum_digit = sum - 10;
        *carry = 1;
    }
    else {
        sum_digit = sum;
        *carry = 0;
    }

    printf("\n");
    printf("        Lưu %d vào kết quả, được kết quả mới là %0*d.\n", 
                                sum_digit, step, saved);
    if (*carry == 1) {
        printf("        Ghi nhớ 1.\n");
    }
}


// sum(stn1, stn2) take two "string" numbers, and output a new string that
// correspond to their sum
// requires: stn1 and stn2 contain only numbers, positve (not asserted)
// effect: produce output, allocates memory (user must free)
// time: O(n), where n is the length of the produced sum
char *sum(const char *stn1, const char *stn2) {
    int first_len = strlen(stn1);
    int second_len = strlen(stn2);
    int sum_len = 0;

    // find the approximate length of the sum of two numbers
    if (first_len >= second_len) {
        sum_len = first_len+1;
    } else {
        sum_len = second_len+1;
    }

    sum_len = sum_len+1; // account for null terminator
    char *result_r = malloc(sum_len*sizeof(char));
    char *stn1_r = reverseNum(stn1);
    char *stn2_r = reverseNum(stn2);

    int step = 1;
    int carry = 0;
    int ten_multiplier = 1;
    long saved = 0;


    for (int i = 0; i < sum_len-1; i++) {
        int digit_1 = getDigit(stn1_r, i);
        int digit_2 = getDigit(stn2_r, i);
        int sum_digit = digit_1+digit_2+carry;
        if (i == sum_len - 2 && sum_digit == 0) {
            result_r[sum_len-2] = '\0';
            break;
        }

        if (sum_digit > 9) {
            sum_digit = sum_digit - 10;
        }

        saved = saved + sum_digit * ten_multiplier;
        printLog(step, digit_1, digit_2, &carry, saved);
        result_r[i] = sum_digit+'0';
        step++;
        ten_multiplier*=10;
    }

    if (result_r[sum_len-2] != 0) {
        result_r[sum_len-1] = '\0';
    }

    free(stn1_r);
    free(stn2_r);
    return reverseNum(result_r);
}




int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Invalid input. Use: ./MyBigNumber [str_num1] [str_num2].\n");
        return -1;
    }
    char **input = malloc(3*sizeof(char *));
    int start = 0;
    while (start < argc) {
        input[start] = argv[start];
        start++;
    }
    char *result = sum(argv[1], argv[2]);
    printf("The sum of two numbers is %s.\n", result);
    free(result);
}