## Name
Project Add 2 numbers

## Description
The program will take in two number arguments as a string, and output its sum, along with step-by-step instruction (in Vietnamese)

## Usage
Download the file MyBigNumber from repository to the desired directory in the computer. Use the following command (in Terminal or Window
directory) to execute the program:

    ./MyBigNumber [stn1] [stn2]

where stn1 and stn2 are numbers in string form.
For example, 

    ./MyBigNumber "1234" "789"

would produce the following message:

    Bước 1: Lấy 4 cộng với 9 được 13.
        Lưu 3 vào kết quả, được kết quả mới là 3.
        Ghi nhớ 1.
    Bước 2: Lấy 3 cộng với 8 được 11. Cộng tiếp với nhớ 1 được 12
        Lưu 2 vào kết quả, được kết quả mới là 23.
        Ghi nhớ 1.
    Bước 3: Lấy 2 cộng với 7 được 9. Cộng tiếp với nhớ 1 được 10
        Lưu 0 vào kết quả, được kết quả mới là 023.
        Ghi nhớ 1.
    Bước 4: Lấy 1 cộng với 0 được 1. Cộng tiếp với nhớ 1 được 2
        Lưu 2 vào kết quả, được kết quả mới là 2023.
    The sum of two numbers is 2023.

**Note**: Due to current memory constraint, this program only run effectively on numbers (both inputs and their sum) that are less than (2^32 - 1, or 4294967295)


## Project status
The project will be updated again upon request for larger number inputs.